# OSI model

OSI model used to understand, how data is transferred between computers in a computer network. Usually, computer networks transfer data through Lan cable but when one computer is using windows and the other is using mac then how do these two computers communicate with each other? The simple answer is through OSI. To accomplish the communication between the two architecture, computer. ISO introduces the OSI model in 1984 which has seven layers and each layer is a package of the protocol.

### The seven layers are

1. Application layer

2. Presentation layer
3. Session layer
4. Transport layer
5. Network layer
6. Data Link layer
7. Physical layer


![Markdown Logo](https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_403,h_297/https://electricalacademia.com/wp-content/uploads/2018/12/image-result-for-osi-model-layers-and-its-function.gif)


1. **Application layer** - The application layer is not chrome, firefox. These network applications used the application layer. Network application means the application which uses the internet like email, firefox, skype, etc. All network application uses application layer protocol. The application layer protocols are HTTP,HTTPS,SMTP,FTP,TELNET. The application's layer protocols that are using by web browsers are HTTP and HTTPS. The application layer that is using for mail is SMTP. File transfer is done with help of FTP protocol.

2. **Presentation layer** - The presentation layer is generally used in translation when the application layer transfers data to the presentation layer the data is in the form of the alphabet or the numerical presentation layer translate this data into the binary form and compress the data and the compression for data is either lossy or lossless then this data is encrypted and also decrypted by SSL (Secure sockets layer).

3. **Session layer** - The session layer helps in managing and enabling the connection and also managing the sending and receiving of data and also manage the termination of the connection. The session layer has its helper called API(Application Program Interface). Just before the session layer helps in managing and enabling the connection and also managing the sending and receiving of data and also manage the termination of the connection. The session layer has its helper called API(Application Program Interface). Just before the connection, the server performs the authentication and if we type the correct password and user name then the connection is established between the server and the computer. The session layer keeps track of the downloaded file and if we request the downloaded file then another session is created for it. All the files in the session layer are in the form of data packets. The session layer helps in session management.
 
4. **Transport layer** - The transport layer controls the reliability of the segmentation, flow control. In the transport layer, the session layer sends the data and the transport layer divides the data into segments and each segment has the port number sequence number data unit and all three are helping to find the correct data for sending to the receiver

5. **Network layer** - The transport layer passes data to the network layer, the network layer receives the data units and passes from one computer to another, the data units reside in the network layer called data packets. The function of the network layer is logical addressing, path determination, and routings. The network layer assigns the IP address to each segment and an IP address is provided because of assurance that each data segment reaches the correct destination. Routing is used to send the data packets from source to destination. For sending the data from source to destination we have many paths and the path determination choose the best possible path using OSPF, BGP, etc.

6. **Data Link layer** - The data packets received from the network layer has an IP address associated with them and the logical addressing has been done at the network layer and physical addressing at the data link layer and the MAC address assigns to each data packet.

7. **Physical Layer** - Till now data is segmented by the transport layer and placed into packets by the network layer and frame by the data link layer which is in the binary form physical layer covert this binary sequence into a signal which is transmitted over media and the media sends the signals and convert it's to bit and pass the bit like a frame to the data link layer finally data is moved into the application layer and in this way the OSI model works.


**References**- https://www.youtube.com/watch?v=vv4y_uOneC0